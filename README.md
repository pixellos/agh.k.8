# Kompilatory 
## AGH Niestacjonarne, semestr 8.

![](img/2021-04-25-21-36-37.png)


### Autorzy:
- Mateusz Popielarz
- Dawid Komisarczuk

## Spis treści
- [1. Specifikacja gramatyki języka w notacji wybranego narzędzia](#1-specifikacja-gramatyki-języka-w-notacji-wybranego-narzędzia)
  - [1.1 Struktura oraz opis kodu źródłowego podzbioru C](#11-struktura-oraz-opis-kodu-źródłowego-podzbioru-c)
  - [1.2 Top-Level members](#12-top-level-members)
  - [1.3 Klasy](#13-klasy)
  - [1.4 Interfejsy](#14-interfejsy)
  - [1.5 Property (Właściwość)](#15-property-właściwość)
  - [1.6 Fieldy (Pola)](#16-fieldy-pola)
  - [1.7 Metody](#17-metody)
  - [1.8 Dziedziczenie/Implementacja](#18-dziedziczenieimplementacja)
- [2. Opis systemu typizacji dla tłumaczonego języka](#2-opis-systemu-typizacji-dla-tłumaczonego-języka)
- [3. Uzasadnienie wybrania konkretnego generatora parserów](#3-uzasadnienie-wybrania-konkretnego-generatora-parserów)
- [4. Opis napotkanych problemów, sposób ich rozwiązania](#4-opis-napotkanych-problemów-sposób-ich-rozwiązania)
  - [Rekursywne dopasowanie naiwasów](#rekursywne-dopasowanie-naiwasów)
  - [Testowanie, automatyzacja ich sprawdzania](#testowanie-automatyzacja-ich-sprawdzania)
- [5. Bibliografia](#5-bibliografia)

<div style="page-break-after: always;"></div>

# 1. Specifikacja gramatyki języka w notacji wybranego narzędzia 

Naszą gramatykę oparliśmy o podzbiór języka C#. Skupiliśmy się na najważniejszych podstawowych konstrukcjach bez zagłębiania w bardziej zaawansowane tematy (np. analizy ciała metod).


## 1.1 Struktura oraz opis kodu źródłowego podzbioru C#

Język składa się z `słów kluczowych`, `znaków specjalnych` i `literałów` oddzielonych `znakami białymi`

![](img/2021-04-25-21-49-53.png)
>  Powyższe konstrukty zdefiniowane w naszym lekserze

<div style="page-break-after: always;"></div>

## 1.2 Top-Level members 

W C# istnieje koncepcja `namespace` - pozwala ona grupować `klasy`, `interfejsy` i `struktury`, aby nazwy nie konfliktowały ze sobą. W naszym projekcie skupiamy się na pojedynczej `przestrzeni nazw` zawierającej wiele `klas` oraz `interfejsów`.

<div style="page-break-after: always;"></div>

## 1.3 Klasy

Każda klasa składa się z 
- Modyfikatora widoczności (domyślnie `internal`)
- `class`
- Nazwy
- Ciała

![](img/2021-04-25-22-15-50.png)
>  Przykładowa `klasa`

![](img/2021-04-25-22-16-46.png)
>  Gramatyka opisująca `klasę`

`Klasa` ma swoje ciało, które zaczyna się `{` i kończy `}`. Pomiędzy nawiasami klamrowymi znajdują się byty określane mianem `member`.

<div style="page-break-after: always;"></div>

## 1.4 Interfejsy

W C# `interfejs` zawiera definicje dla grupy powiązanych `funkcji`, które muszą być zaimplementowane przez `klasę` nieabstrakcyjną (lub strukturę).

`Interfejs` składa się z:
- Modyfikatora widoczności (domyślnie `internal`)
- `interface`
- Nazwy
- Ciała

![](img/Interface.PNG)
>  Przykład `interface`

![](img/InterfaceGrammar.PNG)
>  Kod gramatyki `interface`

Interfejs ma swoje ciało, które zaczyna się `{` i kończy `}`. Pomiędzy nawiasami klamrowymi znajdują się byty określane mianem `member`.

<div style="page-break-after: always;"></div>

## 1.5 Property (Właściwość)

W C# `Property` jest to cukier składniowy na 2 metody, `get_${NazwaProperty}` i `set_${NazwaProperty}`.

`Property` składa się z: 
- Modyfikatora widoczności (domyślnie `private`)
- Typu
- Nazwy
- Metody `Getter`, metody `Setter` lub obu na raz

![](img/2021-04-25-22-23-39.png)
>  Przykład `Property`

![](img/2021-04-25-22-24-28.png)
>  Kod gramatyki `Property`

<div style="page-break-after: always;"></div>

## 1.6 Fieldy (Pola)

W C# `Field` jest to zmienna dowolnego typu, który jest zadeklarowany bezpośrednio w `klasie` (lub `strukturze`).

Składa się ona z 
- Modyfikatora widoczności (domyślnie `private`)
- Typu
- Nazwy
- Średnika

![](img/Field.PNG)
>  Przykład `Field`

![](img/FieldGrammar.PNG)
>  Kod gramatyki `Field`

<div style="page-break-after: always;"></div>

## 1.7 Metody

W C# `Method` to blok kodu, który zawiera serię instrukcji.

Składa się ona z:
- Modyfikatora widoczności (domyślnie `private`)
- Typu zwracanego
- Nazwy
- Listy argumentów
- Ciała

![](img/Method.PNG)
>  Przykład `Method`

![](img/MethodGrammar.PNG)
>  Kod gramatyki `Method`

<div style="page-break-after: always;"></div>

## 1.8 Dziedziczenie/Implementacja

W C# `klasy` mogą rozszerzać inne `klasy` oraz implementować `interfejsy`.  `Interfejsy` mogą również implementować inne `interfejsy`.

Deklarowane jest to za pomocą operatora '`:`'

![](img/Extends.PNG)
> Przykład 


![](img/ExtendsGrammar.PNG)
> Kod gramatyki

<div style="page-break-after: always;"></div>

# 2. Opis systemu typizacji dla tłumaczonego języka

C# jest językiem silnie typowanym. Każda zmienna i stała ma w sobie informacje o typie. 
Typami w `C#` mogą być `Klasy`, `Interfejsy`, `Struktry` i typy wbudowane `void`.

W naszej implementacji, która obejmuje tylko podzbiór języka, zajęliśmy się `Klasami` i `Interfejsami`.

`Klasy` mogą dziedziczyć dokładnie po jednej `klasie`, natomiast gdy nie jest ona jawnie podana, wtedy ma miejsce niejawnie dziedzicznie po `Object`. 
Powoduje to, że każda klasa dziedziczy w którymś punkcie w hierarchii po `Object`.

![](2021-04-27-18-33-28.png)
> Przykład w naszym rozwiązaniu klasy dziedziczącej po innej klasie

![](2021-04-27-18-39-38.png)
> Bardziej złożony przykład

![](2021-04-27-18-57-03.png)
> Dla czytelności rozwiązania pominęliśmy metody w klasach potomnych, które nie są w `C#` określane jako `ExactBinding` - to znaczy takie, które nie należą do tej klasy.

Jako, że każda klasa dziedziczy po `Object`, pominęliśmy to na naszych diagramach.

`Klasa` może implementować jeden lub wiele interfejsów. 

![](img/InterfaceWithMethod.PNG)
> Przykład implementacji interfejsu w pliku wynikowym

![](2021-04-27-18-47-47.png)
> Przykładowa implementacja wielu interfejsów

<div style="page-break-after: always;"></div>

# 3. Uzasadnienie wybrania konkretnego generatora parserów

Jako generator parserów wybraliśmy bibliotekę ANTLR4 w wersji targetującej C# [(link do dokumentacji)](https://github.com/antlr/antlr4/blob/master/doc/csharp-target.md).

Podjęliśmy decyzję, że projekt zostanie napisany w `C#`, głównie po to, aby ten sam kod, który jest kompilowany przez domyślny kompilator `C#` - `Roslyn`, mógł zostać użyty jako wejście do programu.

W ten sposób mogliśmy się upwenić, że nasze pliki wejściowe są zgodne z regułami języka `C#`.


![](2021-04-26-20-59-36.png)
> Plik z `memberami` dołączony do kompilacji, który jest jednocześnie kopiowany do katalogu aplikacji w celu uruchamiania go w testach.

Sama biblioteka jest bardzo prosta w użyciu, ale wymaga instalacji Javy.

Podczas działania uruchamiany jest proces `ANTRL` w Javie oraz kompilator `Roslyn` przez `target`, czyli kawałek kodu działający przy akcjach takich jak `build` czy `clean`

![](2021-04-26-21-13-33.png)
> Okno paczek z zainstalowaną paczką `ANTRL4` i z `targetami`


Dużym plusem było też duże wsparcie dla `ANTRL4` oferowane przez dodatkowe wtyczki, co pozwalało na kolorowanie składni oraz na pokazywanie błędów w IDE.

![](2021-04-26-21-15-44.png)
> Strona dodatku https://marketplace.visualstudio.com/items?itemName=KenDomino.AntlrVSIX

![](2021-04-26-21-16-37.png)
> Przykład kolorowania składni

<div style="page-break-after: always;"></div>

# 4. Opis napotkanych problemów, sposób ich rozwiązania

## Rekursywne dopasowanie naiwasów

Pewną jednostkę czasu zajęło przygotowanie rekursywnej obsługi nawiasów wąsowych

![](2021-04-27-19-13-00.png)
> Przykład problematycznego kodu 

Problem został rozwiązany najprostszym sposobem - za pomocą rekursji ogonowej

![](2021-04-27-19-17-38.png)
> Fragment gramatyki obrazujący rekurencję

<div style="page-break-after: always;"></div>

## Testowanie, automatyzacja ich sprawdzania

Do testów użyta została biblioteka `XUnit`, same testy zostały napisane w `C#`.

![](2021-04-27-17-52-17.png)
> Przykładowy test sprawdzający `Właściwości`

Aby ułatwić sobie życie, a także aby mieć pewność, że wprowadzone zmiany nie wprowadziły błędów w kodzie, w serwisie `gitlab` zostawiony został potok `CI/CD`, który był wykonywany po każdorazowym wrzuceniu kodu do repozytorium.

![](2021-04-27-18-00-09.png)
> Widok z serwisu GitLab w naszym projekcie z potokami https://gitlab.com/pixellos/agh.k.8/-/pipelines

![](2021-04-27-18-01-16.png)
> Weryfikacja testów przez CI/CD

Takie podejście kilka razy pozwoliło nam dostrzec, że zmiany poczynione w kodzie spodowowały błędy. Błędy te lokalnie pozostały niezauważone, natomiast dzięki wykryciu przez CI/CD zostały szybko naprawione i nie zepsuły dotychczasowych prac na głównej gałęzi kodu.

<div style="page-break-after: always;"></div>

# 5. Bibliografia
- https://tomassetti.me/antlr-mega-tutorial/ - poradnik do antlr
- https://code-maze.com/csharp-basics-access-modifiers/
- https://github.com/antlr/antlr4/blob/master/doc/csharp-target.md
- https://github.com/antlr/antlr4
- https://plantuml.com/
- http://plantuml.com/guide
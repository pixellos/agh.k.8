using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using agh.k._8;
using dotnetcore.PlantUml.Generator;
using FluentAssertions;
using Xunit;

namespace dotnetcore
{
    public class IntegrationTests
    {
        private static readonly bool _renderAndShowImage = false;

        private static async Task<string> GenerateUmlFromFile(string name)
        {
            var file = new FileInfo("TestFiles/" + name + ".cs");
            var csharpCode = await File.ReadAllTextAsync(file.FullName);
            var algorithm = new Algorithm(new Visitor());
            var csharpObjects = algorithm.Invoke(csharpCode);
            var plantUmlCode = new PlantUmlGenerator().Generate(csharpObjects);
            return plantUmlCode;
        }

        [Fact]
        public async Task PublicEmptyClass()
        {
            var fileName = "PublicEmptyClass";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class PublicEmptyClass {\n}\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task PublicClassWithPublicField()
        {
            var fileName = "PublicClassWithPublicField";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class PublicClassWithPublicField {\n+string PublicField\n}\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task PublicClassWithGetSetProperty()
        {
            var fileName = "PublicClassWithGetSetProperty";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class PublicClassWithGetSetProperty {\n" +
                                     "+object Property\n" +
                                     "+object GetProperty()\n" +
                                     "+SetProperty(object Property)\n" +
                                     "}\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task ClassExtendingAnotherClass()
        {
            var fileName = "ClassExtendingAnotherClass";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class ClassExtendingAnotherClass {\n}\n" +
                                     "+class ClassBeingExtended {\n}\n" +
                                     "ClassBeingExtended <|-- ClassExtendingAnotherClass\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task MultipleLevelExtending()
        {
            var fileName = "MultipleLevelExtending";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class TheBaseClass {\n}\n" +
                                     "+class FirstExtendingLevel {\n}\n" +
                                     "+class SecondExtendingLevel {\n}\n" +
                                     "TheBaseClass <|-- FirstExtendingLevel\n" +
                                     "FirstExtendingLevel <|-- SecondExtendingLevel\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task ClassWithMethod()
        {
            var fileName = "ClassWithMethod";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class ClassWithMethod {\n" +
                                     "+string Method(object parameter)\n" +
                                     "}\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task SimpleInterface()
        {
            var fileName = "SimpleInterface";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+interface SimpleInterface {\n}\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task ImplementInterface()
        {
            var fileName = "ImplementInterface";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class ImplementInterface {\n}\n" +
                                     "+interface TheInterface {\n}\n" +
                                     "TheInterface <|-- ImplementInterface\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task ImplementInterfaceWithMethodTest()
        {
            var fileName = nameof(ImplementInterfaceWithMethod);

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class ImplementInterfaceWithMethod {\n" +
                                     "+void TheMethod()\n" +
                                     "}\n" +
                                     "+interface InterfaceWithMethod {\n" +
                                     "+void TheMethod()\n" +
                                     "}\n" +
                                     "InterfaceWithMethod <|-- ImplementInterfaceWithMethod\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task ClassWithAnotherClassField()
        {
            var fileName = "ClassWithAnotherClassField";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class ClassWithAnotherClassField {\n" +
                                     "-SomeDependency _someDependency\n" +
                                     "}\n" +
                                     "+class SomeDependency {\n}\n" +
                                     "ClassWithAnotherClassField --> SomeDependency\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task AssociationDependency()
        {
            var fileName = "AssociationDependency";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class AssociationDependency {\n" +
                                     "-AssociatedClass associatedClass\n" +
                                     "}\n" +
                                     "+class AssociatedClass {\n}\n" +
                                     "AssociationDependency --> AssociatedClass\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task MethodWithDependency()
        {
            var fileName = "MethodWithDependency";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            plantUmlCode.Should().Be("+class MethodWithDependency {\n" +
                                     "+void TheMethodWithDependency(DependencyFromMethod theDependency)\n" +
                                     "}\n" +
                                     "+class DependencyFromMethod {\n}\n" +
                                     "MethodWithDependency ..> DependencyFromMethod\n");
            await HandleRenderAndShowImage(plantUmlCode);
        }

        [Fact]
        public async Task FinalResult()
        {
            var fileName = "FinalResult";

            var plantUmlCode = await GenerateUmlFromFile(fileName);

            var file = new FileInfo("TestFiles/FinalResult.plantuml");
            var expected = await File.ReadAllTextAsync(file.FullName);

            plantUmlCode.Should().Be(expected);
            await HandleRenderAndShowImage(plantUmlCode);
        }

        private static async Task HandleRenderAndShowImage(string plantUmlCode)
        {
            if (_renderAndShowImage)
            {
                // Prevents from filesystem errors when utilizing tests
                Random random = new Random();
                Thread.Sleep(random.Next(5, 25));
                var render = await PlantUmlRenderer.Render(plantUmlCode);
                Process.Start(@"cmd.exe ", @$"/c {render}");
            }
        }

    }
}

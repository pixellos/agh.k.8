using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using agh.k._8;
using dotnetcore.PlantUml.Generator;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace dotnetcore
{
    public class FileReadingTests : TestBase
    {
        private readonly ITestOutputHelper _testOutputHelper;

        private readonly PlantUmlGenerator _plantUmlGenerator = new();

        public FileReadingTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        public static IEnumerable<object[]> Data =>
            (new[]
            {
                nameof(ClassWithInterface),
                nameof(TwoClassesEnter),
                nameof(ClassWithProperty),
                nameof(ClassWithField),
                nameof(TwoClasses),
                nameof(ClassWithMethod),
                nameof(ClassWithMethodWithFor),
                nameof(ThreeClasses),
            }).Select(x => (new[] { x }));

        [Theory]
        [InlineData(nameof(ISomeInterface))]
        public async Task WillReadInterface(string name)
        {
            var code = await WillReadFile(name);
            var ns = Apply(code);
            ns.Interfaces.Should().Contain(x => x.Name == name);
        }

        [Theory]
        [MemberData(nameof(Data))]
        public async Task<NameSpace> WillReadClass(string name)
        {
            var csharpCode = await WillReadFile(name);
            var csharpObjects = Apply(csharpCode);
            csharpObjects.Classes.Should().NotBeEmpty();
            csharpObjects.Classes.Should().Contain(x => x.Name == name);
            return csharpObjects;
        }

        [Theory]
        [InlineData(nameof(ClassWithInterface), nameof(ISomeInterface1), nameof(ISomeInterfaceAnother2))]
        public async Task WillReadImplList(string name, params string[] implement)
        {
            var c = await WillReadClass(name);
            c.Classes.Single().ExtendList.Should().Contain(implement);
        }

        private async Task<string> WillReadFile(string name)
        {
            var file = new FileInfo("TestFiles" + "/" + name + ".cs");
            file.Exists.Should().BeTrue();
            var csharpCode = await File.ReadAllTextAsync(file.FullName);
            _testOutputHelper.WriteLine(csharpCode);
            return csharpCode;
        }

        [Theory]
        [InlineData(nameof(ClassWithMethodVoid2), nameof(ClassWithMethodVoid2.Test1))]
        [InlineData(nameof(ClassWithMethodVoid2), nameof(ClassWithMethodVoid2.Test2))]
        [InlineData(nameof(ClassWithMethodWithFor), nameof(ClassWithMethodWithFor.Test))]
        [InlineData(nameof(ClassWithMethod), nameof(ClassWithMethod.Method))]
        [InlineData(nameof(ClassWithMethodOneTwoParmeters), nameof(ClassWithMethodOneTwoParmeters.OneParameter))]
        [InlineData(nameof(ClassWithMethodOneTwoParmeters), nameof(ClassWithMethodOneTwoParmeters.Two))]
        [InlineData(nameof(ClassWithMethodOneTwoParmeters), nameof(ClassWithMethodOneTwoParmeters.Two2))]
        [InlineData(nameof(ClassWithMethodWithFor2), nameof(ClassWithMethodWithFor2.OneParameter))]
        [InlineData(nameof(ClassWithMethodWithFor2), nameof(ClassWithMethodWithFor2.TestDoubleOnlyFor))]
        [InlineData(nameof(ClassWithMethodFor3), nameof(ClassWithMethodFor3.TestOnlyWhile))]
        [InlineData(nameof(ClassWithMethodFor3), nameof(ClassWithMethodFor3.Test))]
        [InlineData(nameof(ClassWithMethodFor3), nameof(ClassWithMethodFor3.Test2))]
        [InlineData(nameof(ClassWithMethodFor3), nameof(ClassWithMethodFor3.Test3))]
        public async Task WillContainMethod(string @class, string methodName)
        {
            var wrc = await WillReadClass(@class);
            wrc.Classes.Should().Contain(x => x.Methods.Any(m => m.Name == methodName));
        }

        [Fact]
        public async Task WillReadPropertyInInterface()
        {
            var c = await WillReadClass(nameof(ClassWithInterface));
            c.Interfaces.Should()
                .ContainSingle(
                    x =>
                        x.Name == nameof(ISomeInterface1) &&
                        x.Properties.Any(p => p.Name == nameof(ISomeInterface1.PropertyAnother))
                );
            c.Classes.Should().ContainSingle(x => x.ExtendList.Contains(nameof(ISomeInterface1)) && x.ExtendList.Contains(nameof(ISomeInterfaceAnother2)));
        }

        [MemberData(nameof(Data))]
        [Theory]
        public async Task WillE2e(string name)
        {
            _testOutputHelper.WriteLine("Input");
            var ns = await WillReadClass(name);
            var plantGraph = _plantUmlGenerator.Generate(ns);
            _testOutputHelper.WriteLine("Plant");
            _testOutputHelper.WriteLine(plantGraph);
            var render = await PlantUmlRenderer.Render(plantGraph);
            _testOutputHelper.WriteLine("Render");
            _testOutputHelper.WriteLine(render);
        }
    }
}

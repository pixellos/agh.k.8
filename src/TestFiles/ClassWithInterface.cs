﻿public interface ISomeInterface1
{
    public string PropertyAnother { get; }

}

public interface ISomeInterfaceAnother2
{
    public string PropertyAnother2 { get; }

}


public class ClassWithInterface : ISomeInterface1, ISomeInterfaceAnother2
{
    public string PropertyAnother { get; }
    public string PropertyAnother2 { get; }
}
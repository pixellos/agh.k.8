﻿public class FinalResult : ClassBeingExtendedX
{
    private ReferencedClass privateField;

    public float ProrpertyWithGetSet { get; set; }

    public char PropertyWithGet { get; }

    public void publicMethod()
    {
        while (true)
        {
            for (var i = 0; i < 5; i++)
            {
                var test = new string[] { };
            }
        }
    }
}

public class ClassBeingExtendedX
{
    protected void methodReferencingClass(MethodReferencedClass methodReferencedClass)
    {
        for (;;)
        {
            while (true)
            {

            }
            while (true)
            {

            }
        }
    }
    private void anotherMethodReferencingClass(AnotherMethodReferencedClass anotherMethodReferencedClass)
    {
    }
}

public class ReferencedClass
{
    protected void protectedMethod2(string input)
    {
    }
}

public class MethodReferencedClass
{
    protected string protectedField;

    public double publicField;

    private void privateMethod(int x)
    {
    }
}

public class AnotherMethodReferencedClass : AnInterface, AnotherInterface
{
    public void InterfaceMethod()
    {

    }

    public int CountStringsContaining(string charSequence)
    {
        return -1;
    }

    public float CountPizzaDiameter(double r)
    {
        return -1.0f;
    }
}

public interface AnInterface
{
    public void InterfaceMethod();

    public int CountStringsContaining(string charSequence);
}

public interface AnotherInterface
{
    public float CountPizzaDiameter(double r);
}

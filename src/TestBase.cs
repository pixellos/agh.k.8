﻿using Antlr4.Runtime.Tree;
using System;
using System.Linq;
using System.Text;

namespace agh.k._8
{
    public class TestBase
    {
        public const string CLASSNAME = "CCC1";
        public const string CLASSNAME2 = "CCCC2";
        public const string ClassNameStartsLodash = "_Class";
        public const string ClassNameStartsAt = "@Class";
        public const string CLASSNAME3 = "c3";
        public const string PROPERTYNAME = "p1";
        public const string PROPERTYNAME2 = "P2";
        public const string PROPERTYNAME3 = "P@";

        protected virtual IParseTreeVisitor<IAtom> Visitor => new Visitor();
        protected virtual IAlgorithm Algorithm => new Algorithm(Visitor);
        public NameSpace Apply(string @class)
        {
            var model = Algorithm.Invoke(@class);
            return model;
        }


        public string CreateClass(string name = CLASSNAME, string modifier = null, string[] extendTypes = default, params string[] body)
        {
            return $"{modifier ?? String.Empty} " +
                $"class {name} " +
                (extendTypes != null ? $": {String.Join(", ", extendTypes)}" : "") +
                "{" +
                    string.Join(string.Empty, body) +
                "}";
        }

        public string Method(string modifier = "private", string returnType = "void", string name = "Method", params (string, string)[] parmeters)
        {
            var sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append(returnType);
            sb.Append(" ");
            sb.Append(name);
            sb.Append(" ");
            sb.Append("(");
            sb.Append(parmeters.Select(x => $"{x.Item1} {x.Item2}").Join());
            sb.Append(")");
            sb.Append("{");
            sb.Append("}");
            return sb.ToString();
        }

        public string CreatePropertyGet(string name, string type = "string", string modifier = "public")
        {
            return CreateProperty("get;", name, type, modifier);
        }

        public string CreateField(string name, string type = "string", string modifier = "public")
        {
            var modifierSpaced = modifier != null ? $"{modifier} " : String.Empty;
            return $"{modifierSpaced}{type} {name};";
        }
        public string CreatePropertySet(string name, string type = "string", string modifier = "public")
        {
            return CreateProperty("set;", name, type, modifier);
        }

        public string CreateProperty(string body, string name, string type = "string", string modifier = "public")
        {
            var modifierSpaced = modifier != null ? $"{modifier} " : String.Empty;
            return $"{modifierSpaced}{type} {name} {{{body}}}";
        }
    }
}
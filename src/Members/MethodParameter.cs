﻿namespace agh.k._8
{
    public class MethodParameter: IAtom
    {
        public string Name { get; set; }
        public string Type { get; set; }

    }
}

﻿namespace agh.k._8
{
    public record Interface(
        string Name,
        Visibility Visibility,
        string[] ExtendList,
        Property[] Properties,
        Method[] Methods
        ) : IAtom;
}
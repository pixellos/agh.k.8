﻿namespace agh.k._8
{
    public enum Visibility
    {
        Internal,
        Public,
        Private
    }
}

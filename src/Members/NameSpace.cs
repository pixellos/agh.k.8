﻿using System.Collections.Generic;

namespace agh.k._8
{
    public enum Modifier
    {
        @public,
        @private,
        @internal,
        @protected
    }

    public interface IAtom
    {

    }

    public record NameSpace(
         List<Class> Classes = default,
         List<Interface> Interfaces = default
        ) : IAtom
    {

        /// <summary>
        ///  Should be reference aware
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public NameSpace Merge(NameSpace o)
        {
            return this;
        }
    }
}

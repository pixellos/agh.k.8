﻿using System.Collections.Generic;

namespace agh.k._8
{
    public class Class : IAtom
    {
        public string Name { get; set; }
        public Visibility Visibility { get; set; }
        public bool IsSealed { get; set; }
        public IReadOnlyCollection<string> ExtendList { get; set; } = new string[0];
        public IReadOnlyCollection<Property> Properties { get; set; } = new Property[0];
        public IReadOnlyCollection<Method> Methods { get; set; } = new Method[0];
        public IReadOnlyCollection<Field> Fields { get; set; } = new Field[0];
    }
}
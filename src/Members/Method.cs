﻿using System.Collections.Generic;

namespace agh.k._8
{
    public class Method : IAtom
    {
        public string Name { get; set; }
        public string ReturnType { get; set; }
        public ClassMemberVisibility Visibility { get; set; }
        public IReadOnlyCollection<MethodParameter> MethodParameters { get; set; }
    }
}

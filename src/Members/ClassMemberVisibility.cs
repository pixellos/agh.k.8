﻿namespace agh.k._8
{
    public enum ClassMemberVisibility
    {
        Private,
        Internal,
        Public,
        Protected,
        ProtectedInternal
    }
}

﻿namespace agh.k._8
{
    public class Field: IAtom
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public ClassMemberVisibility Visibility { get; set; }
    }
}

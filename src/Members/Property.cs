﻿using System.Collections;

namespace agh.k._8
{
    public class Property : IAtom
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public ClassMemberVisibility Visibility{ get; set; }
        public bool IsGet { get; set; }
        public bool IsSet { get; set; }
    }
}

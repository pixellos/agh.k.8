﻿using Antlr4.Runtime.Tree;


namespace agh.k._8
{
    public interface IAlgorithm
    {
        IParseTreeVisitor<IAtom> Visitors { get; }

        NameSpace Invoke(string text);
    }
}

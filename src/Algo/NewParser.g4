parser grammar NewParser;

options { tokenVocab=NewLexer; }

compilation_unit: ((def_class | def_interface) (WH)*)+;

type: NAME;
return_type: NAME;
parameter_type: NAME;
parameter_name: NAME;
parameter_type_and_name:
	WH* parameter_type WH+ parameter_name WH*
	;
parameter_type_and_names:
	WH* 
	| parameter_type_and_name (SEPARATOR WH* parameter_type_and_name)?;

method_def:  MODIFIER WH* ( STATIC WH* )? return_type WH* NAME WH? START_PARAM_LIST 
	parameter_type_and_names
	END_PARAM_LIST WH* ;

method: method_def	
	rec_brackets
; 

rec_brackets:
	INIT_LIST_START rec_brackets_rec* INIT_LIST_END
;

rec_brackets_rec:
	(NAME | 
	START_PARAM_LIST | 
	END_PARAM_LIST | 
	WH | 
	EOL | 
	ASSIGN | 
	PLUS | 
	MINUS | 
	LT | 
	INIT_LIST_START rec_brackets_rec* INIT_LIST_END)+
;


field: MODIFIER? WH+ type WH+ NAME EOL WH*;

property : MODIFIER WH+ 
type WH+
NAME WH*
	INIT_LIST_START 
		WH*
        (GET | GET WH* SET | SET WH* GET | SET) 
		WH* 
	INIT_LIST_END
WH*;

class_base
	: EXTENDS  WH* type (WH* SEPARATOR WH* type)* WH*
	;


class_body: (property | method | field)*;

def_class: MODIFIER? WH* 
    CLASS_IDENTIFIER WH* NAME WH* class_base?
	INIT_LIST_START
		WH*
		class_body
		WH*
	INIT_LIST_END;

// Interface

interface_method_def: method_def (WH)* EOL;
interface_body: (property | interface_method_def)*;


def_interface: MODIFIER? WH* 
    INTERFACE_IDENTIFIER WH* NAME WH* class_base?
	INIT_LIST_START
		WH*
		interface_body
		WH*
	INIT_LIST_END;

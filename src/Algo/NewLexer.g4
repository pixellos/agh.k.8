
lexer grammar NewLexer;

@lexer::header { using System.Collections.Generic; }

// HELPERY
fragment LOWERCASE: [a-z] ;
fragment UPPERCASE: [A-Z] ;

// public class Test 
// {
//    
// }
fragment PUBLIC: 'public';
fragment PRIVATE: 'private';
fragment INTERNAL: 'internal';
fragment PROTECTEDINTERNAL: 'protected internal';
fragment PROTECTED: 'protected';
STATIC: 'static';

fragment NEWLINE : ('\r'? '\n' | '\r' | '\r\n')+;
WH : (NEWLINE | ' ' | '\t') ;

WHITESPACES:   (WH | NEWLINE)+            -> channel(HIDDEN);

MODIFIER: (PUBLIC | PRIVATE | INTERNAL | PROTECTEDINTERNAL | PROTECTED);

fragment EOL_ : ';';
EOL: EOL_;

// Class
CLASS_IDENTIFIER : 'class';
INTERFACE_IDENTIFIER : 'interface';
ASSIGN : '=';
PLUS : '+';
MINUS : '-';
LT : '<';
EXTENDS: ':';
SEPARATOR: ',';


NAME: [a-zA-Z_@][a-zA-Z0-9@]*;

// Accessors fragment
GET: 'get;';
SET: 'set;';

INIT_LIST_START: '{';
INIT_LIST_END: '}';


// Methods

START_PARAM_LIST: '(';
END_PARAM_LIST: ')';
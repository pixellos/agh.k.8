﻿using System;
using System.Linq;
using Antlr4.Runtime.Misc;

namespace agh.k._8
{
    public class Visitor : NewParserBaseVisitor<IAtom>
    {
        public override IAtom VisitDef_interface([NotNull] NewParser.Def_interfaceContext context)
        {
            var visibility = GetVisibility(context.MODIFIER(), Visibility.Internal);
            var current = new Interface(
                        context.NAME()?.GetText(),
                        visibility,
                        null,
                        null,
                        null
                        );
            context.interface_body()?.property()
               .Select(SelectProperty)
               .CollectAny(prop => current = current with { Properties = prop.ToArray() });

            context.interface_body()?.interface_method_def()
                  .Select(x => CreateMethodFrom(x.method_def()))
                  .CollectAny(prop => current = current with { Methods = prop.ToArray() });
            return
                new NameSpace(
                    null,
                    new[] { current }.ToList()
                    );
        }

        public override IAtom VisitDef_class([NotNull] NewParser.Def_classContext context)
        {
            var visibility = GetVisibility(context.MODIFIER(), Visibility.Internal);
            var currentClass = new Class()
            {
                Name = context.NAME().GetText(),
                ExtendList = context.class_base()?.type()?.Select(x => x.NAME().GetText()).ToList() ?? new(),
                Visibility = visibility
            };
            context.class_body()?.property()
                .Select(SelectProperty)
                .CollectAny(prop => currentClass.Properties = prop.ToArray());

            context.class_body()?.method()
                .Select(x => CreateMethodFrom(x.method_def()))
                .CollectAny(prop => currentClass.Methods = prop.ToArray());

            context.class_body()?.field()
                .Select(
                x => new Field
                {
                    Name = x.NAME()?.GetText(),
                    Type = x.type()?.GetText(),
                    Visibility = Enum.TryParse<ClassMemberVisibility>(x.MODIFIER()?.GetText(), true, out var modifier)
                        ? modifier
                        : default,
                })
                .CollectAny(fields => currentClass.Fields = fields.ToArray())
                ;

            return new NameSpace
            (
                new[] { currentClass }.ToList()
            );
        }

        private static Method CreateMethodFrom(NewParser.Method_defContext x)
        {
            return new Method()
            {
                Name = x?.NAME()?.GetText(),
                ReturnType = x?.return_type()?.NAME()?.GetText(),
                Visibility = GetVisibility<ClassMemberVisibility>(x?.MODIFIER(), default),
                MethodParameters = x.parameter_type_and_names()?.parameter_type_and_name()?.Select(
                                    x => new MethodParameter
                                    {
                                        Name = x.parameter_name()?.GetText(),
                                        Type = x.parameter_type()?.GetText()
                                    }).ToArray()
            };
        }

        private static Property SelectProperty(NewParser.PropertyContext x)
        {
            return new Property()
            {
                IsGet = x.GET() != null,
                IsSet = x.SET() != null,
                Name = x.NAME()?.GetText(),
                Type = x.type()?.NAME()?.GetText(),
                Visibility = GetVisibility(x.MODIFIER(), default(ClassMemberVisibility))
            };
        }

        private static TVisibility GetVisibility<TVisibility>(Antlr4.Runtime.Tree.ITerminalNode modifier, TVisibility @default)
            where TVisibility : struct
        {
            var modifierText = modifier?.GetText();
            var parsed = Enum.TryParse<TVisibility>(modifierText, true, out var vis);
            var visibility = parsed ? vis : @default;
            return visibility;
        }

        protected override IAtom AggregateResult(IAtom aggregate, IAtom nextResult)
        {
            return aggregate.AggregateResults(nextResult);
        }
    }
}
﻿using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

namespace agh.k._8
{
    class Algorithm : IAlgorithm
    {
        public Algorithm(IParseTreeVisitor<IAtom> visitors)
        {
            Visitors = visitors;
        }

        public IParseTreeVisitor<IAtom> Visitors { get; }

        public NameSpace Invoke(string text)
        {
            var stream = CharStreams.fromString(text);
            var lexer = new NewLexer(stream);
            var tokens = new CommonTokenStream(lexer);
            var parser = new NewParser(tokens);
            parser.AddErrorListener(new ErrorListener());
            parser.BuildParseTree = true;
            var tree = parser.compilation_unit();
            if (tree.exception != null)
            {
                throw tree.exception;
            }
            var temp = Visitors.Visit(tree);
            return temp as NameSpace ?? throw new System.Exception();
        }
    }
}

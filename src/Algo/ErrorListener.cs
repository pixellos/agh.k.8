﻿using Antlr4.Runtime;
using System.IO;

namespace agh.k._8
{
    class ErrorListener : IAntlrErrorListener<IToken>
    {
        public void SyntaxError(TextWriter output, IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            if (e == null)
            {
                throw new System.Exception(
                    Newtonsoft.Json.JsonConvert.SerializeObject(new
                    {
                        offendingSymbol.Type,
                        offendingSymbol.Text,
                        offendingSymbol.Column,
                        line,
                        charPositionInLine,
                        msg
                    })
                );
            }
            throw e;
        }
    }
}

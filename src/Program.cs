﻿using System.Diagnostics;
using dotnetcore.PlantUml.Generator;

namespace agh.k._8
{
    class Program
    {
        static void Main(string[] args)
        {
            var algorithm = new Algorithm(new Visitor());
            var model = algorithm.Invoke("");
            var uml = new PlantUmlGenerator().Generate(model);
            var rendered = PlantUmlRenderer.Render(uml);
            Process.Start(@"cmd.exe ", @$"/c {rendered}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace agh.k._8
{
    public static class Extensions
    {
        public static void CollectAny<T>(this IEnumerable<T>? items, Action<IEnumerable<T>> action)
        {
            if (items?.Any() ?? false)
            {
                action(items);
            }
        }

        public static String Join(this IEnumerable<String> ss, string separator = ", ")
        {
            return String.Join(separator, ss);
        }

        public static string ToUmlCode(this ClassMemberVisibility classMemberVisibility) =>
            classMemberVisibility switch
            {
                ClassMemberVisibility.Private => "-",
                ClassMemberVisibility.Internal => "#",
                ClassMemberVisibility.Public => "+",
                ClassMemberVisibility.Protected => "#",
                ClassMemberVisibility.ProtectedInternal => "#"
            };

        public static string ToUmlCode(this Visibility visibility) =>
            visibility switch
            {
                Visibility.Public => "+",
                Visibility.Private => "-",
                _ => ""
            };
    }
}

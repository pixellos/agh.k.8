﻿using System.IO;
using System.Threading.Tasks;
using PlantUml.Net;

namespace agh.k._8
{
    /// <summary>
    /// Renders image file from PlantUml source code
    /// </summary>
    public class PlantUmlRenderer
    {
        public static async Task<string> Render(string plantUmlCode)
        {
            var factory = new RendererFactory();
            var renderer = factory.CreateRenderer(new PlantUmlSettings());
            var bytes = await renderer.RenderAsync(plantUmlCode, OutputFormat.Png);
            var path = new FileInfo(plantUmlCode.GetHashCode() + ".png");
            await File.WriteAllBytesAsync(path.FullName, bytes);
            return path.FullName;
        }
    }
}
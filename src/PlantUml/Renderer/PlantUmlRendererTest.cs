﻿using System.Collections.Generic;
using System.Diagnostics;
using agh.k._8;
using dotnetcore.PlantUml.Generator;
using Xunit;

namespace dotnetcore.PlantUml.Renderer
{
    public class PlantUmlRendererTest
    {
        private IPlantUmlGenerator _plantUmlGenerator = new PlantUmlGenerator();

        [Fact(Skip = "Currently only for manual usage")]
        public async void RenderSampleClass()
        {
            var generated = _plantUmlGenerator.Generate(prepareNameSpace());
            var render = await PlantUmlRenderer.Render(generated);
            Process.Start(@"cmd.exe ", @$"/c {render}");
        }

        private static NameSpace prepareNameSpace()
        {
            return new(
                new List<Class>
                {
                    new()
                    {
                        Name = "TestClass"
                    }
                }
            );
        }
    }
}

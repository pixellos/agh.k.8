﻿using System.Collections.Generic;
using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers
{
    public static class PropertyMapper
    {
        internal static string Map(IEnumerable<Property> properties)
        {
            var mappedProperties = properties.Select(MapProperty);
            var mapperPropertiesUml = mappedProperties.Any()
                ? string.Join("\n", mappedProperties) + "\n"
                : "";

            return mapperPropertiesUml;
        }

        private static string MapProperty(Property property) =>
            $"{property.Visibility.ToUmlCode()}{property.Type} {property.Name}";

    }
}

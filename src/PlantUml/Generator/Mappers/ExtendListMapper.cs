﻿using System.Collections.Generic;
using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers
{
    public static class ExtendListMapper
    {
        public static string Map(NameSpace @namespace)
        {
            if (@namespace.Classes == null)
                return "";

            var extendListUml = MapExtendLists(@namespace);
            var joinedExtendListUml = extendListUml.Any()
                ? string.Join("\n", extendListUml) + "\n"
                : "";
            return joinedExtendListUml;
        }

        private static IEnumerable<string> MapExtendLists(NameSpace @namespace) =>
            @namespace.Classes
                .Where(@class => @class.ExtendList.Any())
                .Select(@class => MapExtendList(@class.Name, @class.ExtendList));

        private static string MapExtendList(string className, IEnumerable<string> extendList)
        {
            var mappedExtendList = extendList.Select(
                extendElement => MapExtendElement(className, extendElement));

            return string.Join("\n", mappedExtendList);
        }

        private static string MapExtendElement(string className, string extendedElement) =>
            $"{extendedElement} <|-- {className}";
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers
{
    internal static class FieldsMapper
    {
        internal static string Map(IEnumerable<Field> fields)
        {
            var mappedFields = fields.Select(MapField);
            var mappedFieldsUml = mappedFields.Any()
                ? string.Join("\n", mappedFields) + "\n"
                : "";

            return mappedFieldsUml;
        }

        private static string MapField(Field field) =>
            $"{field.Visibility.ToUmlCode()}{field.Type} {field.Name}";
    }
}

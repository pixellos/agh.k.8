﻿using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers
{
    public static class ClassMapper
    {
        public static string Map(NameSpace @namespace)
        {
            if (@namespace.Classes == null)
                return "";

            var classesUml = @namespace.Classes.Select(GenerateClass);
            var joinedClassesUml = string.Join("\n", classesUml) + "\n";
            return joinedClassesUml;
        }

        private static string GenerateClass(Class @class)
        {
            var fields = FieldsMapper.Map(@class.Fields);
            var properties = PropertyMapper.Map(@class.Properties);
            var methods = MethodMapper.Map(@class);

            return $"{@class.Visibility.ToUmlCode()}class {@class.Name} {{\n"
                   + fields
                   + properties
                   + methods
                   + "}";
        }
    }
}

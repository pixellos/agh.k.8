﻿using System.Collections.Generic;
using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers.Dependency
{
    public class MethodDependencyMapper : BaseDependencyMapper
    {
        public static string Map(NameSpace @namespace)
        {
            if (@namespace.Classes == null)
                return "";

            var mappedClasses = @namespace.Classes
                .Where(@class => @class.Methods.Any())
                .Where(@class => @class.Methods
                    .Any(method => method.MethodParameters
                        .Any(parameter => !ExcludedTypes.Contains(parameter.Type))))
                .Select(@class => MapMethods(@class, @class.Methods));
            var mappedClassesUml = mappedClasses.Any()
                ? mappedClasses.Join("\n") + "\n"
                : "";

            return mappedClassesUml;
        }

        public static string MapMethods(Class @class, IReadOnlyCollection<Method> methods)
        {
            var mappedMethods = methods
                .Select(method => MapMethod(@class, method));
            var mappedMethodsUml = methods.Any()
                ? mappedMethods.Join("\n")
                : "";

            return mappedMethodsUml;
        }

        public static string MapMethod(Class @class, Method method)
        {
            var methodParameters = method.MethodParameters
                .Where(parameter => !ExcludedTypes.Contains(parameter.Type))
                .Select(methodParameter => MapMethodParameter(@class, methodParameter));
            var methodParametersUml = methodParameters.Any()
                ? methodParameters.Join("\n")
                : "";

            return methodParametersUml;
        }

        public static string MapMethodParameter(Class @class, MethodParameter method) =>
            $"{@class.Name} ..> {method.Type}";
    }
}

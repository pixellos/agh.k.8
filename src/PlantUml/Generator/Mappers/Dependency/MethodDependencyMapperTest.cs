﻿using System.Collections.Generic;
using System.Linq;
using agh.k._8;
using FluentAssertions;
using Xunit;

namespace dotnetcore.PlantUml.Generator.Mappers.Dependency
{
    public class MethodDependencyMapperTest
    {
        [Fact]
        public void MapSingleFieldTest()
        {
            // given
            var methodParameter = new MethodParameter { Type = "ParameterType", Name = "ParameterName" };
            var @class = new Class
            {
                Name = "TheClass",
                Methods = new[]
                {
                    new Method
                    {
                        ReturnType = "void", Name = "TestMethod", Visibility = ClassMemberVisibility.Public,
                        MethodParameters = new[] { methodParameter }
                    }
                }
            };

            // when
            var mapMethodParameter = MethodDependencyMapper.MapMethodParameter(@class, @class.Methods.First().MethodParameters.First());

            // then
            mapMethodParameter.Should().Be($"{@class.Name} ..> {methodParameter.Type}");
        }

        [Fact]
        public void MapMethodWithTwoParameters()
        {
            // given
            var methodParameter1 = new MethodParameter { Type = "ParameterType", Name = "ParameterName" };
            var methodParameter2 = new MethodParameter { Type = "ParameterType2", Name = "ParameterName2" };
            var method = new Method
            {
                ReturnType = "void", Name = "TestMethod", Visibility = ClassMemberVisibility.Public,
                MethodParameters = new[] { methodParameter1, methodParameter2 }
            };
            var @class = new Class { Name = "TheClass", Methods = new[] { method } };

            // when
            var mapMethodParameter = MethodDependencyMapper.MapMethod(@class, @class.Methods.First());

            // then
            mapMethodParameter.Should().Be($"{@class.Name} ..> {methodParameter1.Type}\n" +
                                           $"{@class.Name} ..> {methodParameter2.Type}");
        }

        [Fact]
        public void MapTwoMethodsWithTwoParameters()
        {
            // given
            var methodParameter1 = new MethodParameter { Type = "ParameterType1", Name = "ParameterName1" };
            var methodParameter2 = new MethodParameter { Type = "ParameterType2", Name = "ParameterName2" };
            var method1 = new Method
            {
                ReturnType = "void", Name = "TestMethod1", Visibility = ClassMemberVisibility.Public,
                MethodParameters = new[] { methodParameter1, methodParameter2 }
            };
            var methodParameter3 = new MethodParameter { Type = "ParameterType4", Name = "ParameterName3" };
            var methodParameter4 = new MethodParameter { Type = "ParameterType4", Name = "ParameterName4" };
            var method2 = new Method
            {
                ReturnType = "void", Name = "TestMethod2", Visibility = ClassMemberVisibility.Public,
                MethodParameters = new[] { methodParameter3, methodParameter4 }
            };
            var @class = new Class { Name = "TheClass", Methods = new[] { method1, method2 } };

            // when
            var mapMethodParameter = MethodDependencyMapper.MapMethods(@class, @class.Methods);

            // then
            mapMethodParameter.Should().Be($"{@class.Name} ..> {methodParameter1.Type}\n" +
                                           $"{@class.Name} ..> {methodParameter2.Type}\n" +
                                           $"{@class.Name} ..> {methodParameter3.Type}\n" +
                                           $"{@class.Name} ..> {methodParameter4.Type}");
        }

        [Fact]
        public void MapTwoMapsFromTwoClasses()
        {
            // given
            var nameSpace = new NameSpace
            {
                Classes = new List<Class>(new[]
                {
                    new Class
                    {
                        Name = "TestClass1", Methods = new[]
                        {
                            new Method
                            {
                                Name = "Method1", Visibility = ClassMemberVisibility.Public, ReturnType = "void",
                                MethodParameters = new[] { new MethodParameter { Name = "MethodParameter1", Type = "MethodParameterType1" } }
                            }
                        }
                    },
                    new Class
                    {
                        Name = "TestClass2",
                        Methods = new[]
                        {
                            new Method
                            {
                                Name = "Method2", Visibility = ClassMemberVisibility.Public, ReturnType = "void",
                                MethodParameters = new[] { new MethodParameter { Name = "MethodParameter2", Type = "MethodParameterType2" } }
                            }
                        }
                    }
                })
            };

            // when
            var result = MethodDependencyMapper.Map(nameSpace);

            // then
            result.Should().Be("TestClass1 ..> MethodParameterType1\n" +
                               "TestClass2 ..> MethodParameterType2\n");
        }
    }
}

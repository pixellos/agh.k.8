﻿using System.Collections.Generic;
using System.Linq;
using agh.k._8;
using FluentAssertions;
using Xunit;

namespace dotnetcore.PlantUml.Generator.Mappers.Dependency
{
    public class DependencyMapperTest
    {
        [Fact]
        public void MapSingleFieldTest()
        {
            var @class = new Class
            {
                Fields = new[] { new Field { Name = "referencedField", Type = "ReferencedClass", Visibility = ClassMemberVisibility.Public } },
                Name = "TheClass"
            };

            var result = FieldDependencyMapper.MapField(@class, @class.Fields.First());

            result.Should().Be("TheClass --> ReferencedClass");
        }

        [Fact]
        public void MapTwoFilesTest()
        {
            var @class = new Class
            {
                Fields = new[]
                {
                    new Field { Name = "referencedField", Type = "ReferencedClass", Visibility = ClassMemberVisibility.Public },
                    new Field { Name = "referencedField2", Type = "ReferencedClass2", Visibility = ClassMemberVisibility.Public }
                },
                Name = "TheClass"
            };

            var result = FieldDependencyMapper.MapClass(@class);

            result.Should().Be("TheClass --> ReferencedClass\n" +
                               "TheClass --> ReferencedClass2");
        }

        [Fact]
        public void ReferencesExclusionTest()
        {
            var @class = new Class
            {
                Fields = new[]
                {
                    new Field { Name = "referencedField", Type = "ReferencedClass", Visibility = ClassMemberVisibility.Public },
                    new Field { Name = "stringField", Type = "string", Visibility = ClassMemberVisibility.Public },
                    new Field { Name = "referencedField2", Type = "ReferencedClass2", Visibility = ClassMemberVisibility.Public },
                    new Field { Name = "intField", Type = "int", Visibility = ClassMemberVisibility.Public },
                },
                Name = "TheClass"
            };
            NameSpace nameSpace = new NameSpace { Classes = new List<Class> { @class } };

            var result = FieldDependencyMapper.Map(nameSpace);

            result.Should().Be("TheClass --> ReferencedClass\n" +
                               "TheClass --> ReferencedClass2\n");
        }
    }
}

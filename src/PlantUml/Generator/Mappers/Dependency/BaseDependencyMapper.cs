﻿using System.Collections.Generic;

namespace dotnetcore.PlantUml.Generator.Mappers.Dependency
{
    public class BaseDependencyMapper
    {
        protected static readonly List<string> ExcludedTypes = new(new[] { "string", "int", "double", "float", "char", "object" });
    }
}

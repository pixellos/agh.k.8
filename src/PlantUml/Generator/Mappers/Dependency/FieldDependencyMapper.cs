﻿using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers.Dependency
{
    public class FieldDependencyMapper : BaseDependencyMapper
    {
        public static string Map(NameSpace @namespace)
        {
            if (@namespace.Classes == null)
                return "";

            var mapClass = @namespace.Classes
                .Where(@class => @class.Fields.Any(field => !ExcludedTypes.Contains(field.Type)))
                .Select(MapClass);
            var mapClassUml = mapClass.Any()
                ? string.Join("\n", mapClass) + "\n"
                : "";
            return mapClassUml;
        }

        public static string MapClass(Class @class)
        {
            var mapFields = @class.Fields
                .Where(field => !ExcludedTypes.Contains(field.Type))
                .Select(field => MapField(@class, field));
            var mapFieldsUml = mapFields.Any()
                ? string.Join("\n", mapFields)
                : "";
            return mapFieldsUml;
        }

        public static string MapField(Class @class, Field field) =>
            $"{@class.Name} --> {field.Type}";
    }
}

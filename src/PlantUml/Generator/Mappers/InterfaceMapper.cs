﻿using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers
{
    public class InterfaceMapper
    {
        public static string Map(NameSpace @namespace)
        {
            if (@namespace.Interfaces == null)
                return "";

            var interfacesUml = @namespace.Interfaces.Select(GenerateInterface);
            var joinedInterfacesUml = interfacesUml.Any()
                ? string.Join("\n", interfacesUml) + "\n"
                : "";
            return joinedInterfacesUml;
        }

        private static string GenerateInterface(Interface @interface)
        {
            // var fields = FieldsMapper.Map(@interface.Fields);
            // var properties = PropertyMapper.Map(@interface.Properties);
            var methods = MethodMapper.Map(@interface);

            return $"{@interface.Visibility.ToUmlCode()}interface {@interface.Name} {{\n"
                   // + fields
                   // + properties
                   + methods
                   + "}";
        }
    }
}

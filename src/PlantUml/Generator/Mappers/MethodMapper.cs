﻿using System.Linq;
using agh.k._8;

namespace dotnetcore.PlantUml.Generator.Mappers
{
    public static class MethodMapper
    {
        internal static string Map(Class @class)
        {

            // var inheritedMethods = MapInheritedMethods(nameSpace, @class);

            var methods = @class.Methods.Select(MapMethod);
            var methodsUml = methods.Any()
                ? methods.Join("\n") + "\n"
                : "";

            var gettersSetters = MapGettersSetters(@class);
            var gettersSettersUml = gettersSetters.Any()
                ? gettersSetters + "\n"
                : "";

            return methodsUml +
                   gettersSettersUml;
        }

        private static string MapInheritedMethods(NameSpace nameSpace, Class @class)
        {
            var result = "";

            foreach (var interfaceName in @class.ExtendList)
            {
                var foundInterface = nameSpace.Interfaces.First(@interface => @interface.Name.Equals(interfaceName));

                var methods = foundInterface.Methods.Select(MapMethod);
                var methodsUml = methods.Any()
                    ? methods.Join("\n") + "\n"
                    : "";

                result += methodsUml;
            }

            return result;
        }

        internal static string Map(Interface @interface)
        {
            var methods = @interface.Methods?.Select(MapMethod) ?? new string[] { };
            var methodsUml = methods.Any()
                ? methods.Join("\n") + "\n"
                : "";

            return methodsUml;
        }

        private static string MapMethod(Method method)
        {
            var parameters = method.MethodParameters.Select(MapParameter).Join();
            return $"{method.Visibility.ToUmlCode()}{method.ReturnType} {method.Name}({parameters})";
        }

        private static string MapParameter(MethodParameter parameter) =>
            $"{parameter.Type} {parameter.Name}";

        private static string MapGettersSetters(Class @class)
        {
            var result = "";
            foreach (var property in @class.Properties)
            {
                if (property.IsGet)
                {
                    result += $"+{property.Type} Get{Capitalize(property.Name)}()\n";
                }

                if (property.IsSet)
                {
                    result += $"+Set{Capitalize(property.Name)}({property.Type} {property.Name})\n";
                }
            }

            return result != "" ? result[..^1] : result;
        }

        private static string Capitalize(string @string) => @string.First().ToString().ToUpper() + @string[1..];

    }
}

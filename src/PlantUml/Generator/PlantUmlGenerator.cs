﻿using agh.k._8;
using dotnetcore.PlantUml.Generator.Mappers;
using dotnetcore.PlantUml.Generator.Mappers.Dependency;

namespace dotnetcore.PlantUml.Generator
{
    internal class PlantUmlGenerator : IPlantUmlGenerator
    {
        // C# Objects -> PlantUML source code
        public string Generate(NameSpace @namespace)
        {
            var classes = ClassMapper.Map(@namespace);
            var interfaces = InterfaceMapper.Map(@namespace);
            var extendList = ExtendListMapper.Map(@namespace);
            var fieldDependencies = FieldDependencyMapper.Map(@namespace);
            var methodDependencies = MethodDependencyMapper.Map(@namespace);

            return classes +
                   interfaces +
                   extendList +
                   fieldDependencies +
                   methodDependencies;
        }
    }
}

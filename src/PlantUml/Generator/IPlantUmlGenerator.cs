﻿using agh.k._8;

namespace dotnetcore.PlantUml
{
    public interface IPlantUmlGenerator
    {
        // C# Objects -> PlantUML source code
        public string Generate(NameSpace @namespace);
    }
}
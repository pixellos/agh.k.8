﻿using System;
using System.Linq;

namespace agh.k._8
{
    public static class AtomExtensions
    {
        static public T CopyValues<T>(T target, T source)
        {
            var t = typeof(T);
            var properties = t.GetProperties().Where(prop => prop.CanRead && prop.CanWrite);
            foreach (var prop in properties)
            {
                var value = prop.GetValue(source, null);
                if (value != null)
                    prop.SetValue(target, value, null);
            }
            return target;
        }


        public static IAtom AggregateResults(this IAtom aggregate, IAtom nextResult)
        {
            if (nextResult == null)
            {
                return aggregate;
            }
            if (aggregate == null)
            {
                return nextResult;
            }
            if (aggregate is NameSpace first && nextResult is NameSpace second)
            {
                return first with
                {
                    Classes = (first.Classes ?? new()).Concat(second.Classes ?? new()).ToList(),
                    Interfaces = (first.Interfaces ?? new()).Concat(second.Interfaces ?? new()).ToList()
                };
            }
            else if (aggregate is Class c && nextResult is Class c2)
            {
                return CopyValues(c, c2);
            }
            throw new NotImplementedException();
        }
    }
}

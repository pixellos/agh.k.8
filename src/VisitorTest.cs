using FluentAssertions;
using System;
using System.Collections;
using System.Linq;
using Xunit;

namespace agh.k._8
{
    public class VisitorTest : TestBase
    {
        private const string CATEGORY = "CATEGORY";
        private const string PROPERTIES = "PROPERTIES";
        private const string FIELDS = "FIELDS";
        private const string METHOD = "METHOD";
        private const string CLASS = "CLASS";

        [Fact]
        [Trait(CATEGORY, CLASS)]
        public void TwoClassesNamesWillBeRead()
        {
            var @class = CreateClass(CLASSNAME);
            var @class2 = CreateClass(CLASSNAME3);
            var ns = Apply(@class + " " + @class2);
            ns.Classes.Should().Contain(x => x.Name == TestBase.CLASSNAME);
            ns.Classes.Should().Contain(x => x.Name == TestBase.CLASSNAME3);
        }

        [Fact]
        [Trait(CATEGORY, CLASS)]
        public void TwoClassesNamesWillBeReadSecondOneWillHavePropertyOfFirstType()
        {
            var @class = CreateClass(CLASSNAME);
            var @class2 = CreateClass(CLASSNAME3, body: CreatePropertyGet(PROPERTYNAME2, CLASSNAME));
            var ns = Apply(@class + " " + @class2);
            ns.Classes.Should().Contain(x => x.Name == TestBase.CLASSNAME);
            ns.Classes.Should().Contain(x => x.Name == TestBase.CLASSNAME3);
        }

        [Theory]
        [InlineData(ClassNameStartsAt)]
        [InlineData(CLASSNAME2)]
        [InlineData(ClassNameStartsLodash)]
        [Trait(CATEGORY, CLASS)]
        public void ClassWillReadName(string s)
        {
            var @class = CreateClass(s);
            var ns = Apply(@class);
            ns.Classes.Should().ContainSingle(x => x.Name == s);
        }


        [Theory]
        [InlineData(ClassNameStartsAt, CLASSNAME2)]
        [InlineData(ClassNameStartsLodash, CLASSNAME2)]
        [Trait(CATEGORY, CLASS)]

        public void MultipleClasses(params string[] names)
        {
            var @class = string.Join(" ", names.Select(x => CreateClass(x)));
            var ns = Apply(@class);
            foreach (var name in names)
            {
                ns.Classes.Should().ContainSingle(x => x.Name == name);
            }
        }


        [Fact]
        [Trait(CATEGORY, CLASS)]
        public void ClassWillAcceptExtendList()
        {
            var @class = CreateClass(ClassNameStartsAt, extendTypes: new[] { nameof(IEnumerable) });
            var ns = Apply(@class);
            ns.Classes.Should().ContainSingle(x => x.Name == ClassNameStartsAt && x.ExtendList.Contains(nameof(IEnumerable)));
        }

        [Theory]
        [InlineData("internal", Visibility.Internal)]
        [InlineData("public", Visibility.Public)]
        [InlineData("", Visibility.Internal)]
        [Trait(CATEGORY, CLASS)]
        public void ClassWillAcceptModifiers(string modifier, Visibility visibility)
        {
            var @class = CreateClass(ClassNameStartsAt, modifier: modifier);
            var ns = Apply(@class);
            ns.Classes.Should().ContainSingle(x => x.Name == ClassNameStartsAt && x.Visibility == visibility);
        }

        [Trait(CATEGORY, PROPERTIES)]
        [Fact]
        public void ClassWillReadProperty()
        {
            var @class = CreateClass(CLASSNAME3, body: CreatePropertyGet(PROPERTYNAME));
            var ns = Apply(@class);
            ns.Classes.Single().Properties.Should().ContainSingle(p => p.Name == PROPERTYNAME && p.Visibility == ClassMemberVisibility.Public && p.IsGet);
        }

        [Trait(CATEGORY, METHOD)]
        [Fact]
        public void MethodWithName()
        {
            var @class = CreateClass(CLASSNAME3, body: Method(name: PROPERTYNAME));
            var ns = Apply(@class);
            ns.Classes.Single().Methods.Should().ContainSingle(p => p.Name == PROPERTYNAME);
        }

        [Trait(CATEGORY, METHOD)]
        [Fact]
        public void MethodWithReturnType()
        {
            var @class = CreateClass(CLASSNAME3, body: Method(name: PROPERTYNAME, returnType: nameof(Object)));
            var ns = Apply(@class);
            ns.Classes.Single().Methods.Should().ContainSingle(m => m.Name == PROPERTYNAME && m.ReturnType == nameof(Object));
        }

        [Trait(CATEGORY, METHOD)]
        [Fact]
        public void MethodWithParameters()
        {
            var parameter1 = ("object", "any");
            var parameter2 = ("object2", "any2");
            ; var @class = CreateClass(CLASSNAME3,
                 body: Method(
                     name: PROPERTYNAME,
                     parmeters:
                         new[] {
                            parameter1,
                            parameter2,
                     })
                 );
            var ns = Apply(@class);
            ns.Classes.Single().Methods.Should().ContainSingle(
                m =>
                m.Name == PROPERTYNAME &&
                m.MethodParameters.Count == 2 &&
                m.MethodParameters.Any(mp => mp.Name == parameter1.Item2 && mp.Type == parameter1.Item1) &&
                m.MethodParameters.Any(mp => mp.Name == parameter2.Item2 && mp.Type == parameter2.Item1)
                );
        }


        [Trait(CATEGORY, PROPERTIES)]
        [Fact]
        public void TwoProperties()
        {
            var @class = CreateClass(
                CLASSNAME3,
                body: new[] {
                    CreateField(PROPERTYNAME, modifier: "public"),
                    CreateField(PROPERTYNAME2, type: nameof(Object), modifier: "internal")
                });
            var ns = Apply(@class);
            ns.Classes.Single().Fields.Should().ContainSingle(p => p.Name == PROPERTYNAME && p.Visibility == ClassMemberVisibility.Public);
            ns.Classes.Single().Fields.Should().ContainSingle(p => p.Name == PROPERTYNAME2 && p.Visibility == ClassMemberVisibility.Internal);
        }


        [Trait(CATEGORY, FIELDS)]
        [Fact]
        public void TwoFields()
        {
            var @class = CreateClass(
                CLASSNAME3,
                body: new[] {
                    CreatePropertyGet(PROPERTYNAME, modifier: "public"),
                    CreatePropertySet(PROPERTYNAME2, type: nameof(Object), modifier: "internal")
                });
            var ns = Apply(@class);
            ns.Classes.Single().Properties.Should().ContainSingle(p => p.Name == PROPERTYNAME && p.Visibility == ClassMemberVisibility.Public && p.IsGet);
            ns.Classes.Single().Properties.Should().ContainSingle(p => p.Name == PROPERTYNAME2 && p.Visibility == ClassMemberVisibility.Internal && p.IsSet);
        }
    }
}
